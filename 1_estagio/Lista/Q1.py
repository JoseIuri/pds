import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def function(x):
    return np.cos(2*np.pi*4000*x) + np.cos(2*np.pi*600*x) 


taxa = 8000
Ts = 1/taxa
t0 = 8*10**-4
amostras = 2*t0*taxa

t = np.linspace(-t0, t0, 180*10**3, endpoint=False)
k = np.linspace(-t0, t0, amostras, endpoint=False)

signal = function(t)

signal_amostrado = function(k)

plt.plot(t, signal,'r--', label='Sinal no tempo')

plt.stem(k, signal_amostrado, label='Sinal amostrado')
plt.xlabel('tempo (s)')
plt.ylabel('Sinal')
plt.title('Sinal no Tempo e Sinal Amostrado')
plt.legend()
plt.show()