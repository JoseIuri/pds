import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def function(x):
    return np.cos(2*np.pi*4000*x) + np.cos(2*np.pi*600*x) 

F = 5*56000
T = 1/F
t0 = 5*10**-3
amostras = 2*t0*F

t = np.linspace(-t0, t0, amostras, endpoint=False)

Fs = 6000
Ts = 1/Fs
amostras = 2*t0*Fs
k = np.linspace(-t0, t0, amostras, endpoint=False)


signal = function(t)
signal_amostrado_f = function(k)

signal_intp_M1 = np.zeros((signal_amostrado_f.size)*2)
for i in range (0,signal_amostrado_f.size - 1):
    signal_intp_M1[2*i] = signal_amostrado_f[i]

signal_intp_M2 = np.zeros((signal_amostrado_f.size)*5)
for i in range (0,signal_amostrado_f.size - 1):
    signal_intp_M2[5*i] = signal_amostrado_f[i]

signal_intp_M3 = np.zeros((signal_amostrado_f.size)*10)
for i in range (0,signal_amostrado_f.size - 1):
    signal_intp_M3[10*i] = signal_amostrado_f[i]

N = signal_amostrado_f.size
yf = scipy.fftpack.fft(signal_amostrado_f)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs

plt.subplot(4, 1, 1)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal amostrado')
plt.legend()
plt.title('Espectro dos sinais com interpolação distintas')

N = signal_intp_M1.size
yf = scipy.fftpack.fft(signal_intp_M1)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs*2

plt.subplot(4, 1, 2)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal interpolado M=2')
plt.legend()

N = signal_intp_M2.size
yf = scipy.fftpack.fft(signal_intp_M2)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs*5

plt.subplot(4, 1, 3)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal interpolado M=5')
plt.legend()

N = signal_intp_M3.size
yf = scipy.fftpack.fft(signal_intp_M3)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs*10

plt.subplot(4, 1, 4)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal interpolado M=10')
plt.legend()

plt.xlabel('Frequência (Hz)')
plt.show()