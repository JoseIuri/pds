import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def function(x):
    return np.cos(2*np.pi*4000*x) + np.cos(2*np.pi*600*x) 

F = 5*56000
T = 1/F
t0 = 5*10**-3
amostras = 2*t0*F

t = np.linspace(-t0, t0, amostras, endpoint=False)

Fs = 6000
Ts = 1/Fs
amostras = 2*t0*Fs
k = np.linspace(-t0, t0, amostras, endpoint=False)


signal = function(t)
signal_amostrado_f = function(k)

signal_dec_L1 = signal_amostrado_f[ ::2]
signal_dec_L2 = signal_amostrado_f[ ::5]
signal_dec_L3 = signal_amostrado_f[ ::10]

N = signal_amostrado_f.size
yf = scipy.fftpack.fft(signal_amostrado_f)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs

plt.subplot(4, 1, 1)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal amostrado')
plt.legend()
plt.title('Espectros dos sinais com decimação distintas')

N = signal_dec_L1.size
yf = scipy.fftpack.fft(signal_dec_L1)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs/2

plt.subplot(4, 1, 2)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal decimado L=2')
plt.legend()

N = signal_dec_L2.size
yf = scipy.fftpack.fft(signal_dec_L2)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs/5

plt.subplot(4, 1, 3)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal decimado L=5')
plt.legend()

N = signal_dec_L3.size
yf = scipy.fftpack.fft(signal_dec_L3)
xf = scipy.fftpack.fftfreq(len(yf)) * Fs/10

plt.subplot(4, 1, 4)
plt.stem(xf, (2/N)*np.abs(yf), label='Sinal decimado L=10')
plt.legend()

plt.xlabel('Frequência (Hz)')
plt.show()

