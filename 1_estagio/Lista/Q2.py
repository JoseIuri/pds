import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def function(x):
    return np.cos(2*np.pi*4000*x) + np.cos(2*np.pi*600*x) 

F = 56000
T = 1/F
t0 = 1
amostras = 2*t0*F

t = np.linspace(-t0, t0, amostras, endpoint=False)

Fs = 12000
Ts = 1/Fs
t0 = 5*10**-3
amostras = 2*t0*Fs
k = np.linspace(-t0, t0, amostras, endpoint=False)

signal = function(t)

signal_amostrado = function(k)


N = signal.size
yf = scipy.fftpack.fft(signal)
xf = scipy.fftpack.fftfreq(len(yf)) * F

plt.subplot(2, 1, 1)
plt.plot(xf, (2/N)*np.abs(yf), label='Sinal original')

axes = plt.gca()
axes.set_xlim([-Fs/2,Fs/2])
plt.title('Espectro do Sinal Original')
plt.ylabel('Amplitude')

N = signal_amostrado.size
yf_amostrado= scipy.fftpack.fft(signal_amostrado)
xf = scipy.fftpack.fftfreq(len(yf_amostrado)) * Fs
plt.subplot(2, 1, 2)
plt.stem(xf, (2/N)*np.abs(yf_amostrado), label='Sinal amostrado')
axes = plt.gca()
axes.set_xlim([-Fs/2,Fs/2])
plt.title('Espectro do Sinal Amostrado')
plt.xlabel('Freq (Hz)')
plt.ylabel('Amplitude')


plt.show()