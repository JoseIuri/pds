import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def function(x):
    return np.cos(2*np.pi*4000*x) + np.cos(2*np.pi*600*x) 

F = 5*56000
T = 1/F
t0 = 5*10**-3
amostras = 2*t0*F

t = np.linspace(-t0, t0, amostras, endpoint=False)

Fs = 12000
Ts = 1/Fs
amostras = 2*t0*Fs
k = np.linspace(-t0, t0, amostras, endpoint=False)

Fs2 = Fs/2
Ts2 = 1/Fs2
amostras2 = 2*t0*Fs2
l = np.linspace(-t0, t0, amostras2, endpoint=False)

Fs3 = Fs/4
Ts3 = 1/Fs3
amostras3 = 2*t0*Fs3
m = np.linspace(-t0, t0, amostras3, endpoint=False)


signal = function(t)
signal_amostrado_f = function(k)
signal_amostrado_f2 = function(l)
signal_amostrado_f4 = function(m)

signal_recuperado = 0

for i in range (0, signal_amostrado_f.size-1):
    signal_recuperado = signal_recuperado + signal_amostrado_f[i]*np.sinc((t-k[i])/Ts)

plt.plot(t, signal_recuperado, label='Sinal recuperado F=Fs')

signal_recuperado2 = 0

for i in range (0, signal_amostrado_f2.size-1):
    signal_recuperado2 = signal_recuperado2 + signal_amostrado_f2[i]*np.sinc((t-l[i])/Ts2)

plt.plot(t, signal_recuperado2, label='Sinal recuperado F=Fs/2')

signal_recuperado3 = 0

for i in range (0, signal_amostrado_f4.size-1):
    signal_recuperado3 = signal_recuperado3 + signal_amostrado_f4[i]*np.sinc((t-m[i])/Ts3)

plt.plot(t, signal_recuperado3, label='Sinal recuperado F=Fs/4')

plt.xlabel('tempo (s)')
plt.ylabel('Amplitude do Sinal')
plt.title('Sinais recuperados com frequências de amostragens distintas')
plt.legend()
plt.show()