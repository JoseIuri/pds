import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def function(x):
    return np.cos(2*np.pi*7000*x)

def quantize(x):
    return np.around(x, decimals=1)

F = 5*56000
T = 1/F
t0 = 5*10**-3
amostras = 2*t0*F

t = np.linspace(-t0, t0, amostras, endpoint=False)

Fs = 20000
Ts = 1/Fs
amostras = 2*t0*Fs
k = np.linspace(-t0, t0, amostras, endpoint=False)

signal = function(t)
signal_amostrado_f = function(k)

signal_quantizado = quantize(signal_amostrado_f)

plt.plot(t,signal, label='Sinal Original')
plt.stem(k,signal_amostrado_f, '--', label='Sinal Amostrado Fs=20kHz')
plt.stem(k,signal_quantizado, '--', markerfmt='x', label='Sinal Quantizado')
axes = plt.gca()
axes.set_xlim(-6/7000,6/7000)
plt.xlabel('tempo (s)')
plt.ylabel('Amplitude do Sinal')
plt.title('Sinais amostrados e quantizados')
plt.legend()
plt.show()