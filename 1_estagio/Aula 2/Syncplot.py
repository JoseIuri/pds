import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def sinc(x):
    a = np.zeros(np.prod(x.shape))
    for z in range (0, np.prod(x.shape) - 1):
        if (x[z]==0) :
            a[z] = 1
        else:
            a[z] = np.sin(np.pi*x[z])/(np.pi*x[z])
    
    return a

def function(x):
    return x**2

Ts = 2

t0 = 20
taxa = 50
amostras = 2*t0*taxa
t = np.linspace(-t0, t0, amostras, endpoint=False)
k = np.arange(-t0, t0, Ts)

signal = function(t)
plt.plot(t, signal,'r--')

for i in range (-10, 10):
    signal = function(i*Ts)*sinc(t-i*Ts)
    plt.plot(t, signal)

plt.show()