import pyaudio
import wave
from array import array
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def quantize(x):
    passo = x.max()/1000
    return np.round(x/passo)*passo


FORMAT=pyaudio.paInt16
CHANNELS=2
RATE=44100
CHUNK=1024
RECORD_SECONDS=15
FILE_NAME="VOZ.wav"

audio=pyaudio.PyAudio()

stream=audio.open(format=FORMAT,channels=CHANNELS, 
                  rate=RATE,
                  input=True,
                  frames_per_buffer=CHUNK)

#starting recording
frames=[]

for i in range(0,int(RATE/CHUNK*RECORD_SECONDS)):
    data=stream.read(CHUNK)
    data_chunk=array('h',data)
    vol=max(data_chunk)
    if(vol>=500):
        print("Voz detectada")
        frames.append(data)
    else:
        print("silencio")
    print("\n")


stream.stop_stream()
stream.close()
audio.terminate()

wavfile=wave.open(FILE_NAME,'wb')
wavfile.setnchannels(CHANNELS)
wavfile.setsampwidth(audio.get_sample_size(FORMAT))
wavfile.setframerate(RATE)
wavfile.writeframes(b''.join(frames))
wavfile.close()

from scipy.io import wavfile 

fs, Audiodata = wavfile.read(FILE_NAME)

plt.plot(Audiodata)
plt.title('Sinal de audio no tempo')
plt.show()

print(fs)
Audiodata = quantize(Audiodata)

wavfile=wave.open('QUANTIZADA.wav','wb')
wavfile.setnchannels(CHANNELS)
wavfile.setsampwidth(audio.get_sample_size(FORMAT))
wavfile.setframerate(RATE)
wavfile.writeframes(b''.join(Audiodata))
wavfile.close()

