import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

def function(x):
    return np.cos(2*np.pi*7000*x)

F = 5*56000
T = 1/F
t0 = 5*10**-3
amostras = 2*t0*F

t = np.linspace(-t0, t0, amostras, endpoint=False)

Fs = 2000
Ts = 1/Fs
amostras = 2*t0*Fs
k = np.linspace(-t0, t0, amostras, endpoint=False)

Fs2 = 8000
Ts2 = 1/Fs2
amostras2 = 2*t0*Fs2
l = np.linspace(-t0, t0, amostras2, endpoint=False)

signal = function(t)
signal_amostrado_f = function(k)
signal_amostrado_f2 = function(l)

plt.plot(t,signal, label='Sinal Original')

signal_recuperado = 0

for i in range (0, signal_amostrado_f.size-1):
    signal_recuperado = signal_recuperado + signal_amostrado_f[i]*np.sinc((t-k[i])/Ts)

plt.plot(t, signal_recuperado, label='Sinal recuperado Fs=2kHz')

signal_recuperado2 = 0

for i in range (0, signal_amostrado_f2.size-1):
    signal_recuperado2 = signal_recuperado2 + signal_amostrado_f2[i]*np.sinc((t-l[i])/Ts2)

plt.plot(t, signal_recuperado2, label='Sinal recuperado Fs=8kHz')

axes = plt.gca()
axes.set_xlim(-6/7000,6/7000)
plt.xlabel('tempo (s)')
plt.ylabel('Amplitude do Sinal')
plt.title('Sinais recuperados com frequências de amostragens distintas')
plt.legend()
plt.show()