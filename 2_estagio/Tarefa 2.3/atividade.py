import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

N = 400
Ts = 1 / 30000; 

x = np.linspace(0.0, N*Ts, N)

g = np.cos(2*np.pi*500*x) + np.random.normal(0,0.1,N) #Sinal + ruido

hx = np.convolve(g,g) #Convolucao linear

hxc = np.fft.ifft(np.fft.fft(g) * np.fft.fft(g)) # Uso do teorema da convolucao

plt.subplot(3, 1, 1)
plt.title('Convolução circular x Convolução Linear')
plt.stem(x, g, 'r')
plt.legend(['Sinal original'])

plt.subplot(3,1,2)
plt.stem(range(len(hx)), hx, 'r')
plt.legend(['Convolução linear'])

plt.subplot(3,1,3)
plt.stem(x, hxc, 'r')
plt.legend(['Convolução circular'])

plt.show()