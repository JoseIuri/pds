import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

k1 = np.arange(0,5)
k2 = np.arange(0,3)

g = np.ones(np.size(k1))
h = np.ones(np.size(k2))

y = np.convolve(g,h) # Convolucao linear

k3 = np.arange(0,np.size(y))


plt.subplot(3, 1, 1)
plt.stem(k3,y)
plt.title('Espectro dos sinais reconstruidos')

N=5

G = scipy.fftpack.fft(g, N)
H = scipy.fftpack.fft(h, N)

Y = G*H

y_i = scipy.fftpack.ifft(Y)

k3 = np.arange(0,np.size(y_i))

plt.subplot(3, 1, 2)
plt.stem(k3,y_i)

#Realiza a convolução circular com diferentes pontos
for N in range (5,8):

	y_cir = np.zeros(N)

	h = np.append(h, np.zeros(N-np.size(h)))
	g = np.append(g, np.zeros(N-np.size(g)))

	for n in range(0,N):
		for j in range (0,N):
			y_cir[n] = y_cir[n] + (g[j]*h[(n-j)%N])

	k4 = np.arange(0,np.size(y_cir))
	plt.subplot(5, 1, N-2)
	plt.stem(k4,y_cir)

plt.show()