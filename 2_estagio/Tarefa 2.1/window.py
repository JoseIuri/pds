import numpy as np
from scipy.fftpack import fft, fftfreq, fftshift
from scipy import signal 
import matplotlib.pyplot as plt

# number de pontos no sinal
N = 400
# Periodo de amostragem
T = 1.0 / 10000
x = np.linspace(0.0, N*T, N)
y = np.cos(2*np.pi*100*x)+np.cos(2*np.pi*780*x)+np.cos(2*np.pi*1500*x)
yf = fft(y)

#Gera as janelas
wb = signal.blackman(N)
ywb = fft(y*wb)
wh = signal.hamming(N)
ywh = fft(y*wh)
wg = signal.gaussian(N,std=50)
ywg = fft(y*wg)

xf = fftfreq(N, T)
xf = fftshift(xf)

yplot = fftshift(yf)
plt.subplot(4, 1, 1)
plt.title('FFT realizado com janelas diferentes')
plt.plot(xf, 1.0/N * np.abs(yplot), 'r')
plt.legend(['FFT sem janela'])
plt.xlim(-2500,2500)

yplot = fftshift(ywb)
plt.subplot(4, 1, 2)
plt.plot(xf, 1.0/N * np.abs(yplot), 'b')
plt.legend(['FFT com janela de Blackman'])
plt.xlim(-2500,2500)

yplot = fftshift(ywh)
plt.subplot(4, 1, 3)
plt.plot(xf, 1.0/N * np.abs(yplot), 'g')
plt.legend(['FFT com janela de Hamming'])
plt.xlim(-2500,2500)

yplot = fftshift(ywg)
plt.subplot(4, 1, 4)
plt.plot(xf, 1.0/N * np.abs(yplot), 'm')
plt.legend(['FFT com janela Gaussiana'])
plt.xlabel('Freq (Hz)')
plt.xlim(-2500,2500)

plt.show()