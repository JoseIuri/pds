import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft, fftfreq, fftshift

# Define o sinal
def function(x): 
    return np.cos(2*np.pi*7000*x)

def quantize(x):
    return np.around(x, decimals=1)

F = 5*56000
T = 1/F
t0 = 5*10**-3
amostras = 2*t0*F

t = np.linspace(-t0, t0, amostras, endpoint=False)

Fs = 20000
Ts = 1/Fs
amostras = 2*t0*Fs
k = np.linspace(-t0, t0, amostras, endpoint=False)

signal = function(t)
signal_amostrado = function(k)

signal_quantizado = quantize(signal_amostrado)

# Realiza a fft
yf = fft(signal_quantizado)

# Pega os valores em Hz das frequencias
xf = fftfreq(len(yf), Ts)
xf = fftshift(xf)

yplot = fftshift(yf)

# Plota os graficos
plt.title('FFT de sinal quantizado')

plt.subplot(2,1,1)
plt.plot(t, signal,'r--', label='Sinal no tempo')

plt.stem(k, signal_amostrado, label='Sinal amostrado')
plt.xlabel('tempo (s)')
plt.ylabel('Sinal')
axes = plt.gca()
axes.set_xlim(-6/7000,6/7000)

plt.subplot(2,1,2)
plt.stem(xf, 2.0/amostras * np.abs(yplot), 'r')
plt.xlabel('Freq (Hz)')
plt.show()