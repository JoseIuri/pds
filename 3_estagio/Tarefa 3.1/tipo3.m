% Jose Iuri Barbosa de Brito

clear all

%Tipo 3

M = 52; N = M+1;

Omega_r_b = 2; Omega_r_a = 4; Omega_s = 10;

kr_b = floor(N*Omega_r_b/Omega_s);
kr_a = floor(N*Omega_r_a/Omega_s);

A = [zeros(1,kr_b+1) ones(1,kr_a-kr_b+1) zeros(1,M/2-kr_a+1)];

k = 1:((M)/2);

for n=0:M
    h(n+1) = 2*sum((-1).^(k+1).*A(k+1).*sin(pi.*k*(1+2*n)/N));
end;
h = h./N;

stem(h);

a = 1;
fvtool(h,a)