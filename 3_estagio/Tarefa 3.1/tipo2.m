% Jose Iuri Barbosa de Brito

clear all

%TIPO II

M = 51; N = M+1;

Omega_p = 1; Omega_r = 1.2; Omega_s = 10;

kp = floor(N*Omega_p/Omega_s);
kr = floor(N*Omega_r/Omega_s);

A = [ones(1,kp+1) zeros(1,N/2-kr)];

k = 1:((M-1)/2);

for n=0:M
    h(n+1) = A(1) + 2*sum((-1).^k.*A(k+1).*cos(pi.*k*(1+2*n)/N));
end;
h = h./N;

stem(h);

a = 1;
fvtool(h,a)