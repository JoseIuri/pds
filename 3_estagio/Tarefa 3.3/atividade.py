from scipy import signal
import matplotlib.pyplot as plt
import numpy as np


# Freq de amostragem
fs = 5*10**3

#Design do filtro
b, a = signal.iirdesign(wp = 1.1*10**3/(0.5*fs), ws = 1*10**3/(0.5*fs), gstop= 60, gpass=1, ftype='cheby2')

w, h = signal.freqz(b, a)

# Plot
fig = plt.figure()
plt.title('Resposta em frequência do Filtro Digital')
ax1 = fig.add_subplot(111)

plt.plot(w*fs/(2*np.pi), 20 * np.log10(abs(h)), 'b')
plt.ylabel('Amplitude [dB]', color='b')
plt.xlabel('Frequencia [Hz]')

ax2 = ax1.twinx()
angles = np.unwrap(np.angle(h))
plt.plot(w*fs/(2*np.pi), angles, 'g')
plt.ylabel('Ângulo (rad)', color='g')
plt.grid()
plt.axis('tight')
plt.show()