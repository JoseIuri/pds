import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft, fftfreq, fftshift
from scipy.io import wavfile
from scipy import signal

#Pega o audio original
fs, data = wavfile.read('assovios.wav')

audio = data #Audio original

audio_s = fft(audio)

N = audio.size

xf = fftfreq(N, 1/fs)
xf = fftshift(xf)

# Assobio 1: 2880 2900
# Assobio 2: 2900 2920
# Assobio 3: 2920 2940

b1, a1 = signal.iirdesign(wp = [1750/(0.5*fs), 1825/(0.5*fs)], ws = [1690/(0.5*fs), 1885/(0.5*fs)], gstop= 30, gpass=1, ftype='cheby2')
b2, a2 = signal.iirdesign(wp = [1950/(0.5*fs), 2080/(0.5*fs)], ws = [1890/(0.5*fs), 2140/(0.5*fs)], gstop= 25, gpass=1, ftype='cheby2')
b3, a3 = signal.iirdesign(wp = [2880/(0.5*fs), 2940/(0.5*fs)], ws = [2820/(0.5*fs), 3000/(0.5*fs)], gstop= 30, gpass=1, ftype='cheby2')

w1, h1 = signal.freqz(b1, a1)
w2, h2 = signal.freqz(b2, a2)
w3, h3 = signal.freqz(b3, a3)

# Plot
plt.subplot(3,1,1)
plt.title('Resposta em frequência dos Filtros Digitais')
plt.plot(w1, 20 * np.log10(abs(h1)), 'b')

plt.subplot(3,1,2)
plt.plot(w2, 20 * np.log10(abs(h2)), 'b')

plt.subplot(3,1,3)
plt.plot(w3, 20 * np.log10(abs(h3)), 'b')

plt.show()

zi = signal.lfilter_zi(b1, a1)
y1, _ = signal.lfilter(b1, a1, audio, zi=zi*audio[0])
zi = signal.lfilter_zi(b2, a2)
y2, _ = signal.lfilter(b2, a2, audio, zi=zi*audio[0])
zi = signal.lfilter_zi(b3, a3)
y3, _ = signal.lfilter(b3, a3, audio, zi=zi*audio[0])

y1s = fft(y1)
y2s = fft(y2)
y3s = fft(y3)

plt.subplot (4, 1, 1)
plt.title('Audio Original')
plt.plot(audio, 'r')
plt.subplot (4, 1, 2)
plt.title('Assobio 1')
plt.plot(y1, 'r')
plt.subplot (4, 1, 3)
plt.title('Assobio 2')
plt.plot(y2, 'r')
plt.subplot (4, 1, 4)
plt.title('Assobio 3')
plt.plot(y3, 'r')
plt.show()


yplot = fftshift(audio_s)

plt.subplot(4, 1, 1)
plt.title('Resposta em frequência do áudio original')
plt.plot(xf, 1.0/N * np.abs(yplot), 'r')
plt.xlim(-4000,4000)
plt.grid()

yplot = fftshift(y1s)

plt.subplot(4, 1, 2)
plt.title('Resposta em frequência do assobio 1')
plt.plot(xf, 1.0/N * np.abs(yplot), 'r')
plt.xlim(-4000,4000)
plt.grid()

yplot = fftshift(y2s)

plt.subplot(4, 1, 3)
plt.title('Resposta em frequência do assobio 2')
plt.plot(xf, 1.0/N * np.abs(yplot), 'r')
plt.xlim(-4000,4000)
plt.grid()

yplot = fftshift(y3s)

plt.subplot(4, 1, 4)
plt.title('Assobio 3')
plt.plot(xf, 1.0/N * np.abs(yplot), 'r')
plt.xlim(-4000,4000)
plt.grid()

plt.show()

wavfile.write("assovio1.wav", fs, y1)
wavfile.write("assovio2.wav", fs, y2)
wavfile.write("assovio3.wav", fs, y3)
